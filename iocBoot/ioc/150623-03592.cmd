###############################################################################
#
# PBI system   : Thorlabs FW102 series filter wheel
# Location     : lab
#
# Support      : https://jira.esss.lu.se/projects/PBITECH
# Wiki         : https://confluence.esss.lu.se/display/PBITECH
#
###############################################################################

# location of the system (section-subsection)
# lab example
epicsEnvSet("RACKROW",                      "PBILAB")
# acquisition unit logical name
epicsEnvSet("ACQ_UNIT",                     "FW1")
# acquisition device ID or name
epicsEnvSet("ACQ_DEVID",                    "FW1")
# acquisition device model
epicsEnvSet("ACQ_MODEL",                    "FW102 USB")
# acquisition device serial number (empty means any)
epicsEnvSet("ACQ_SERNO",                    "150623-03592")
# acquisition device serial port
epicsEnvSet("ACQ_SERPORT",                  "/dev/ttyUSB0")

# EPICS_CA_MAX_ARRAY_BYTES: 10 MB max CA request
# epicsEnvSet("EPICS_CA_MAX_ARRAY_BYTES",     "10000000")

# include the main startup file
cd startup
< main.cmd
